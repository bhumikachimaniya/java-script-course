## what is java script ?  

1. java script is a scriptinng language that enables you to create dynamically updating content, control multimedia, animate images, and pretty much everything else.
1. java script run in browser.
1. java script is client side language
1. using java script we can make hybrid apps.
1. it's use as front end as well as back end 
1. java script can calculate ,manipulate and validates data.
1. JavaScript is a dynamic programming language that's used for web development, in web applications, for game development, and lots more.
1. It allows you to implement dynamic features on web pages that cannot be done with only HTML and CSS.
1. many browsers use JavaScript as a scripting language for doing dynamic things on the web.
1. JavaScript is mainly used for web-based applications and web browsers.
1. But JavaScript is also used beyond the Web in software, servers and embedded hardware controls
1.java script was designed simply to run in browser but it is doing more than the time you have the memory .used in the browser for performing variety of web based things in the past but after that javascript has evolved a lot now.

## Role of java script

java sc ript is known as j.s or ECMA . it is a high level dynamic interpeted programming language .its allow client side scripting to create completely dynamic web application and web sites.java script was initially designed for making pages"alive".scripts can be excuted in the browser itself.**java script and java are different programming languages**.it can be executed on the browser asd well as the server.java script is a safe languages when used in browser.


## history of java script ? 

JavaScript was introduced by Brendan Eich in 1995.

It was developed for Netscape 2, and became the ECMA-262 standard in 1997.

mocha,livescript,ECMA script,using java script we can make hybrid apps.
 
## various topic in java script ?

1. introduction of java script 
1. Variable declaration
1. data types
1. object 
1. Operators 
1. Control Statements
1. Error Handling
1. loops
1. Understanding arrays
1. Function Declaration
1. arrays  

## various uses of java script 

 * **Adding interactive behavior to web pages** 

1. Show or hide more information with the click of a button
1. Change the color of a button when the mouse hovers over it
1. Slide through a carousel of images on the homepage
1. Zooming in or zooming out on an image
1. Displaying a timer or count-down on a website
1. Playing audio and video in a web page
1. Displaying animations

* **Building web servers and developing server applications**

Beyond websites and apps, developers can also use JavaScript to build simple web servers and develop the back-end infrastructure using Node.js.    

* **Game development**

* **Creating web and mobile apps**


## what are its advantages?

 JavaScript is a dynamic computer programming language. It is lightweight and most commonly used as a part of the web pages, whose implementation allows a client-side script to interact with a user and to make dynamic pages. It is an interpreted programming language with object-oriented capabilities.

javaScript is relatively simple to learn and implement.
JavaScript is used everywhere on the web.

## what are disadvantages ?

The main problem or disadvantage in JavaScript is that the code is always visible to everyone anyone can view JavaScript code.

If the error occurs in the JavaScript, it can stop to render the whole website. Browsers are extremely tolerant of JavaScript errors.

## How does JavaScript works on a website? 

The web browser loads a web page, parses the HTML, and creates what is known as a Document Object Model (DOM) from the contents. The DOM presents a live view of the web page to your JavaScript code. The browser will then grab everything linked to the HTML, like images and CSS files.

## How does JavaScript Work? 

everything in java script happen inside an execution context.

**memory creation phases**

 memory component  = variable environment , and code component  = thread of excution 

javascript is a synchonous single threaded languages , ( one command at a time ) 

Whenever we run a JavaScript program inside a web browser, JavaScript code is received by the browser's engine and the engine runs the source code to obtain the output. In a standard JavaScript engine, the source code goes through several steps and gets executed.

The majority of web applications work on the server-side. Essentially, this means that their functionality depends on the interaction of your device with a remote server. The device in this case is the client.
Once a connection is established, the server is able to provide information for the client. With the use of the client’s software, the information can then be received in the form of a webpage.
This process might seem rather long though it’s clear why certain web applications like search engines would rely on this blueprint.
In every other scenario, it is easy to see why JavaScript has garnered favorability among developers.
In client-side web applications, the programming language in question is tasked with running inside an Internet browser, known here as a Web client. This client connects to the Web server to retrieve web pages.
The browser is deployed with a built-in interpreter to read and run the code. 

# feature of java script 

* Platform Independent
 * Light Weight Scripting language
 * Dynamic Typing
 * Object-oriented programming support
 * Prototype-based
 * Interpreted Language

### 1. Platform Independent  

This implies that JavaScript is platform-independent or we can say it is portable; which simply means that you can simply write the script once and run it anywhere and anytime. In general, you can write your JavaScript applications and run them on any platform or any browser without affecting the output of the Script.

### 1. light Weight Scripting Language

JavaScript is a lightweight scripting language because it is made for data handling at the browser only. Since it is not a general-purpose language so it has a limited set of libraries. Also as it is only meant for client-side execution and that too for web applications, hence the lightweight nature of JavaScript is a great feature.

### 1. Dynamic Typing 

JavaScript supports dynamic typing which means types of the variable are defined based on the stored value. For example, if you declare a variable x then you can store either a string or a Number type value or an array or an object. This is known as dynamic typing.

To understand this, in languages like Java, we explicitly mention that a particular variable will store a certain type of data, whereas in JavaScript we do not have to provide the data type while declaring a variable. In JavaScript, we just have to use var or let keyword before the variable name to declare a variable without worrying about its type.

### 1. Object-Oriented Programming Support 

Starting from ES6, the concept of class and OOPs has been more refined. Also, in JavaScript, two important principles with OOP in JavaScript are Object Creation patterns (Encapsulation) and Code Reuse patterns (Inheritance). Although JavaScript developers rarely use this feature but its there for everyone to explore.

### 1. Prototype-based Language 

JavaScript is a prototype-based scripting Language. This means javascript uses prototypes instead of classes or inheritance. In languages like Java, we create a class and then we create objects for those classes. But in JavaScript, we define object prototype and then more objects can be created using this object prototype.

### 1. Interpreted Language 

JavaScript is an interpreted language which means the script written inside javascript is processed line by line. These Scripts are interpreted by JavaScript interpreter which is a built-in component of the Web browser. But these days many JavaScript engines in browsers like the V8 engine in chrome uses just in time compilation for JavaScript code.

# why is java script best for web development ?

* Because javascript is not only used front end, but also the back end. This makes the way to become full stack developer more easier.
* JavaScript allows the programmers to build large-scale web application easily. It simplifies the whole process of developing large-scale web applications.
* Developers now adopting serverless platforms to reduce cost by paying for only what they use.

# what is the role of JavaScript in front end development

In front-end web development, JavaScript is one of three major elements that are needed to end up with a web page that can be rendered properly. ... java script's major role is to provide a platform for making the website more interactive, such as by creating dropdown menus and forms.

java script is a text-based programming language used both on the client-side and server-side that allows you to make web pages interactive. ... Incorporating JavaScript improves the user experience of the web page by converting it from a static page into an interactive one. To recap, JavaScript adds behavior to web pages. 

# what is the role of JavaScript in back end development

Backend developers use a type of JavaScript called Node. js for backend work. ... js framework allows a developer to handle data updates from the front end and build scalable network applications able to process many simultaneous user requests, amongst other things.

